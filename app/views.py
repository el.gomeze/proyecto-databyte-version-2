from django.shortcuts import render
from .models import Pagina

# Create your views here.

def index(request):   
    return render(request, 'app/index.html')

def contacto(request):
    return render(request, 'app/contacto.html')

def servicios(request):
    return render(request, 'app/servicios.html')

def galeria(request):  
    paginas = Pagina.objects.all()
    data = {
        'paginas':paginas
    }   
    return render(request, 'app/galeria.html', data)
