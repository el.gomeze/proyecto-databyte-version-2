from django.db import models

# Create your models here.

class ThemePagina(models.Model):
    nombre = models.CharField(max_length=50)
    def __str__(self):
        return self.nombre

class Pagina(models.Model):
    nombre = models.CharField(max_length=50)
    precio = models.IntegerField()
    descripcion = models.TextField()
    BDD = models.BooleanField()
    ThemePagina = models.ForeignKey(ThemePagina, on_delete=models.PROTECT)
    fecha_creacion = models.DateField()
    imagen = models.ImageField(upload_to="paginas", null=True)
    def __str__(self):
        return self.nombre
