from django.contrib import admin
from django.urls import path
from .views import index, contacto, servicios, galeria

urlpatterns = [
    path('', index, name="index"),      
    path('servicios/', servicios, name="servicios"),
    path('contacto/', contacto, name="contacto"),
    path('galeria/', galeria, name="galeria"),
]