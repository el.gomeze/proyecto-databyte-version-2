from django.contrib import admin
from .models import ThemePagina, Pagina

# Register your models here.

admin.site.register(ThemePagina)
admin.site.register(Pagina)
