function contratadoOnePage(){
    alert('El Plan - One Page fue Contratado Éxitosamente! Tu código de Compra es XFG789HJN. Nos Contactaremos contigo en menos de 24 horas para concluir el contrato!');
}

function contratadoMultipagina(){
    alert('El Plan - Multipagina fue Contratado Exitosamente! Tu codigo de Compra es PLJ521DES. Nos Contactaremos contigo en menos de 24 horas para concluir el contrato!');
}

function instagram(){
    window.open("https://www.instagram.com/?hl=es-la");
}

function facebook(){
    window.open("https://www.facebook.com/");
}

function twitter(){
    window.open("https://twitter.com/?lang=es");
}

function dribble(){
    window.open("https://dribbble.com/");
}

function youtube(){
    window.open("https://www.youtube.com/");
}

$(function () {
    $("#formulario-databyte").validate({
        rules: {
            nombres: {
                required: true,
                minlength: 2,
            },
            apellidos: {
                required: true,
                minlength: 2
            },
            direccion: {
                required: true,
                minlength: 2
            },
            region: {
                required: true
            },
            comuna: {
                required: true
            },
            celular: {
                required: true,
                minlength: 9,
            },
            email: {
                required: true,
                email: true
            },
            mensaje: {
                required: true,
                minlength: 10,
            }
        },
        messages: {
            nombres: {
                required: 'Ingrese su Nombre.',
                minlength: 'Largo de nombre insuficiente - Minimo 2 caracteres.'
            },
            apellidos: {
                required: 'Ingrese sus Apellidos.',
                minlength: 'Largo de apellidos insuficiente - Minimo 2 caracteres.'
            },
            direccion: {
                required: 'Ingrese su Direccion.',
                minlength: 'Largo de direccion insuficiente - Minimo 2 caracteres.'
            },
            region: {
                required: 'Debe Escoger una Region del Listado.',
            },
            comuna: {
                required: 'Debe Escoger una Comuna del Listado.',
            },
            celular: {
                required: 'Ingrese su Numero de Celular.',
                minlength: 'Largo de Numero Celular Insuficiente - Minimo 9 digitos.'
            },
            email: {
                required: 'Ingrese su correo electrónico.',
                email: 'Formato de correo no válido. Debe tener @ y una extension. Ejemplo: @.com'
            },
            mensaje: {
                required: 'Ingrese Un Mensaje',
                minlength: 'Largo de Mensaje Insuficiente - Minimo 10 caracteres.'
            }
        }
    });
});